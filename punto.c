#include "punto.h"

int producto_escalar(Punto_t a, Punto_t b)
{
	int producto;
	producto = a.x * b.x + a.y * b.y + a.z * b.z;
	return producto;
}

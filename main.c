#include <stdio.h>
#include "punto.h"

int main()
{
	Punto_t puntos[2];
	printf("Ingrese coordenas x y z separadas por un espcaio...\n");
	for(int i=0;i<2;i++) {
		printf("Por favor, ingrese coordenadas punto %d: ",i+1);
		scanf("%d %d %d",&puntos[i].x, &puntos[i].y, &puntos[i].z);
		printf("x=%d, y=%d, z=%d\n",puntos[i].x, puntos[i].y, puntos[i].z);
	}

	int producto = producto_escalar(puntos[0], puntos[1]);
	printf("Mi producto escalar: %d\n", producto);
}

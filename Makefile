DEPS=punto.h

test: main.o punto.o
	gcc -o test main.o punto.o

main.o: main.c $(DEPS)
	gcc -c main.c

punto.o: punto.c $(DEPS)
	gcc -c punto.c

.PHONY: clean
clean:
	rm *.o test
